﻿using Microsoft.Bot.Builder;
using System;

namespace Bot_Builder_Echo_Bot_V4
{
    public class WelcomeUserState
    {
        public static bool DidBotWelcomeUser { get; set; } = false;
    }

    /// Initializes a new instance of the <see cref="WelcomeUserStateAccessors"/> class.
    public class WelcomeUserStateAccessors
    {
        public WelcomeUserStateAccessors(UserState userState)
        {
            this.UserState = userState ?? throw new ArgumentNullException(nameof(userState));
        }

        /// <summary>
        /// Gets the <see cref="IStatePropertyAccessor{T}"/> name used for the <see cref="CounterState"/> accessor.
        /// </summary>
        /// <remarks>Accessors require a unique name.</remarks>
        /// <value>The accessor name for the counter accessor.</value>
        public static string DidBotWelcomeUser_Name { get; } = $"{nameof(WelcomeUserStateAccessors)}.DidBotWelcomeUser";

        public IStatePropertyAccessor<bool> DidBotWelcomeUser { get; set; }

        public UserState UserState { get; }
    }
}
