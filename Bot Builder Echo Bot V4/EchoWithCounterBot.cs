﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BotBuilderEchoBotV4;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Bot_Builder_Echo_Bot_V4
{
    /// <summary>
    /// Represents a bot that processes incoming activities.
    /// For each user interaction, an instance of this class is created and the OnTurnAsync method is called.
    /// This is a Transient lifetime service.  Transient lifetime services are created
    /// each time they're requested. For each Activity received, a new instance of this
    /// class is created. Objects that are expensive to construct, or have a lifetime
    /// beyond the single turn, should be carefully managed.
    /// For example, the <see cref="MemoryStorage"/> object and associated
    /// <see cref="IStatePropertyAccessor{T}"/> object are created with a singleton lifetime.
    /// </summary>
    /// <seealso cref="https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.1"/>
    public class EchoWithCounterBot : IBot
    {
        private readonly WelcomeUserStateAccessors _welcomeUserStateAccessors;
        private readonly IHttpClientFactory _clientFactory;
        private readonly EchoBotAccessors _echoAccessors;
        private readonly ILogger _logger;

        // Generic message to be sent to user
        private const string _welcomeMessage = "Hello pirate!";

        /// <summary>
        /// Initializes a new instance of the <see cref="EchoWithCounterBot"/> class.
        /// </summary>
        /// <param name="accessors">A class containing <see cref="IStatePropertyAccessor{T}"/> used to manage state.</param>
        /// <param name="loggerFactory">A <see cref="ILoggerFactory"/> that is hooked to the Azure App Service provider.</param>
        /// <seealso cref="https://docs.microsoft.com/en-us/aspnet/core/fundamentals/logging/?view=aspnetcore-2.1#windows-eventlog-provider"/>
        public EchoWithCounterBot(EchoBotAccessors echoAccessors, WelcomeUserStateAccessors welcomeAccessors, ILoggerFactory loggerFactory, IHttpClientFactory clientFactory)
        {
            if (loggerFactory == null)
            {
                throw new System.ArgumentNullException(nameof(loggerFactory));
            }

            _logger = loggerFactory.CreateLogger<EchoWithCounterBot>();
            _logger.LogTrace("EchoBot turn start.");

            _echoAccessors = echoAccessors ?? throw new System.ArgumentNullException(nameof(echoAccessors));
            _welcomeUserStateAccessors = welcomeAccessors ?? throw new System.ArgumentNullException(nameof(welcomeAccessors));
            _clientFactory = clientFactory;
        }

        /// <summary>
        /// Every conversation turn for our Echo Bot will call this method.
        /// There are no dialogs used, since it's "single turn" processing, meaning a single
        /// request and response.
        /// </summary>
        /// <param name="turnContext">A <see cref="ITurnContext"/> containing all the data needed
        /// for processing this conversation turn. </param>
        /// <param name="cancellationToken">(Optional) A <see cref="CancellationToken"/> that can be used by other objects
        /// or threads to receive notice of cancellation.</param>
        /// <returns>A <see cref="Task"/> that represents the work queued to execute.</returns>
        /// <seealso cref="BotStateSet"/>
        /// <seealso cref="ConversationState"/>
        /// <seealso cref="IMiddleware"/>
        public async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Use state accessor to extract the didBotWelcomeUser flag
            var didBotWelcomeUser = await _welcomeUserStateAccessors.DidBotWelcomeUser.GetAsync(turnContext, () => false);

            // Handle Message activity type, which is the main activity type for shown within a conversational interface
            // Message activities may contain text, speech, interactive cards, and binary or unknown attachments.
            // see https://aka.ms/about-bot-activity-message to learn more about the message and other activity types
            if (turnContext.Activity.Type == ActivityTypes.Message)
            {
                var client = _clientFactory.CreateClient();
                var request = new HttpRequestMessage(HttpMethod.Get, "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/53907a18-6305-4938-be57-a745b246edcf?subscription-key=cd9ec7ebf3ca4d8bba86abf82b167bdb&q=" + turnContext.Activity.Text);
                var response = await client.SendAsync(request);
                var result = JsonConvert.DeserializeObject<RootObject>(await response.Content.ReadAsStringAsync());

                if (result.topScoringIntent.score < 0.75)
                {
                    // Match not sufficient.
                    await turnContext.SendActivityAsync("I don't understand what you're saying...", cancellationToken: cancellationToken);
                }
                else
                {

                    switch (result.topScoringIntent.intent)
                    {
                        case "None":
                            await turnContext.SendActivityAsync("You said nothing...", cancellationToken: cancellationToken);
                            break;
                        case "Want to register":
                            await turnContext.SendActivityAsync("You can register on the following topics: ", cancellationToken: cancellationToken);
                            await turnContext.SendActivityAsync("- IOT", cancellationToken: cancellationToken);
                            await turnContext.SendActivityAsync("- Chatbots", cancellationToken: cancellationToken);
                            await turnContext.SendActivityAsync("- Capture the flag", cancellationToken: cancellationToken);
                            await turnContext.SendActivityAsync("- Roslyn", cancellationToken: cancellationToken);
                            break;
                        case "Show titles":
                            await turnContext.SendActivityAsync("I will show you the titles: IOT, Chatbots, Capture the flag, Roslyn", cancellationToken: cancellationToken);
                            break;
                        default:
                            throw new ArgumentNullException(nameof(result.topScoringIntent.intent), "Not supported intent");
                    }
                }
            }
            else if (turnContext.Activity.Type == ActivityTypes.ConversationUpdate) // Greet when users are added to the conversation.
            {
                if (turnContext.Activity.MembersAdded.Any())
                {
                    foreach (var member in turnContext.Activity.MembersAdded)
                    {
                        if (member.Id != turnContext.Activity.Recipient.Id)
                        {
                            await turnContext.SendActivityAsync(_welcomeMessage, cancellationToken: cancellationToken);
                        }
                    }
                }
            }

            // save any state changes made to your state objects.
            await _welcomeUserStateAccessors.UserState.SaveChangesAsync(turnContext);
        }
    }
}
